use std::{fmt::Write, process::ExitStatus, time::Duration};

use anyhow::{anyhow, Result};
use tokio::time::timeout;

use crate::{
    neutrino::{GameState, Player},
    shepherd::EngineHandle,
};

type Version = (i64, i64, i64);
const LATEST_NSN_VERSION: Version = (0, 1, 1);
const LATEST_NAN_VERSION: Version = (1, 1, 0);

const SETUP_TIMEOUT: Duration = Duration::from_millis(2000);
const NEW_GAME_TIMEOUT: Duration = Duration::from_millis(2000);
const STOP_GAME_TIMEOUT: Duration = Duration::from_millis(1000);

pub async fn setup_engine(handle: EngineHandle) -> Result<Idling> {
    let id = handle.process.id();
    match timeout(SETUP_TIMEOUT, setup_engine_inner(handle)).await {
        Ok(v) => v,
        Err(_) => Err(anyhow!("Setup of engine (id = {id:?}) timed out")),
    }
}

pub async fn setup_engine_inner(mut handle: EngineHandle) -> Result<Idling> {
    handle.writeln_noflush("uni 0.1.0").await?;
    handle.writeln_noflush("charset UTF-8").await?;
    handle
        .writeln_noflush(format!("maximum_memory {}", handle.max_mem).as_str())
        .await?;
    handle
        .writeln_noflush(format!("num_cores {}", handle.max_cores).as_str())
        .await?;
    handle.writeln_noflush("uni_hello_end").await?;
    handle.flush().await?;

    let mut capabilities = Capabilities::default();
    let mut authors = Vec::new();
    let mut name = Option::None;
    for setup_line_count in 0.. {
        if setup_line_count > 1000 {
            return Err(anyhow!("Too many lines in hello response from engine"));
        }
        let line = handle.readln().await?;
        let mut tokens = line.split_ascii_whitespace();
        match tokens.next() {
            Some("id") => match tokens.next() {
                Some("name") => {
                    let remainder: Vec<_> = tokens.collect();
                    // TODO(casper): Consider not blindly converting to single-space
                    name.replace(remainder.join(" "));
                }
                Some("author") => {
                    let remainder: Vec<_> = tokens.collect();
                    authors.push(remainder.join(" "));
                }
                other => return Err(anyhow!("Unexpected token after id: {other:?}")),
            },
            Some("capability") => {
                match tokens.next() {
                    Some("nsn_version") => capabilities.nsn_version = parse_version(&mut tokens)?,
                    Some("nan_version") => capabilities.nan_version = parse_version(&mut tokens)?,
                    Some("whitespace") => {
                        capabilities.whitespace_format = parse_whitespace_format(&mut tokens)?
                    }
                    Some("game_state") => {
                        capabilities.game_state_format = parse_game_state_format(&mut tokens)?
                    }
                    Some("partial_turns") => capabilities.partial_turns = parse_bool(&mut tokens)?,
                    Some("stop_go") => capabilities.stop_go = parse_bool(&mut tokens)?,
                    Some("go_infinite") => capabilities.go_infinite = parse_bool(&mut tokens)?,
                    Some("time_control") => capabilities.time_control = parse_bool(&mut tokens)?,
                    Some("tc_increment") => capabilities.tc_increment = parse_bool(&mut tokens)?,
                    Some("tc_delay") => capabilities.tc_delay = parse_bool(&mut tokens)?,
                    Some("tc_overtime") => capabilities.tc_overtime = parse_bool(&mut tokens)?,
                    Some("tc_hourglass") => capabilities.tc_hourglass = parse_bool(&mut tokens)?,
                    Some("tc_max_per_turn") => {
                        capabilities.tc_max_per_turn = parse_bool(&mut tokens)?
                    }
                    Some("configurable_max_depth") => {
                        capabilities.configurable_max_depth = parse_bool(&mut tokens)?
                    }
                    Some("configurable_max_nodes") => {
                        capabilities.configurable_max_nodes = parse_bool(&mut tokens)?
                    }
                    Some("move_limit") => capabilities.move_limit = parse_bool(&mut tokens)?,
                    other => return Err(anyhow!("Unknown capability: {other:?}")),
                };
                if let Some(token) = tokens.next() {
                    return Err(anyhow!("Unexpected token after capability. token: {token}"));
                }
            }
            Some("uni_ok") => break,
            other => return Err(anyhow!("Unexpected token: {other:?}")),
        }
    }

    let name = match name {
        Some(v) => v,
        None => return Err(anyhow!("Engine did not provide a name")),
    };

    let info = EngineInfo {
        name,
        authors,
        capabilities,
    };

    Ok(Idling(Engine { info, handle }))
}

pub struct Capabilities {
    nsn_version: Version,
    nan_version: Version,
    whitespace_format: WhitespaceFormat,
    game_state_format: GameStateFormat,
    partial_turns: bool,
    stop_go: bool,
    go_infinite: bool,
    time_control: bool,
    tc_increment: bool,
    tc_delay: bool,
    tc_overtime: bool,
    tc_hourglass: bool,
    tc_max_per_turn: bool,
    configurable_max_depth: bool,
    configurable_max_nodes: bool,
    move_limit: bool,
}

impl Default for Capabilities {
    fn default() -> Self {
        Self {
            nsn_version: LATEST_NSN_VERSION,
            nan_version: LATEST_NAN_VERSION,
            whitespace_format: Default::default(),
            game_state_format: Default::default(),
            partial_turns: false,
            stop_go: false,
            go_infinite: false,
            time_control: true,
            tc_increment: false,
            tc_delay: false,
            tc_overtime: false,
            tc_hourglass: false,
            tc_max_per_turn: false,
            configurable_max_depth: false,
            configurable_max_nodes: false,
            move_limit: false,
        }
    }
}

impl Capabilities {
    pub fn assert_support(&self, _game_params: &GameParameters) -> Result<()> {
        // TODO(casper): Actually check that the engine supports the game
        Ok(())
    }
}

pub struct EngineInfo {
    pub name: String,
    pub authors: Vec<String>,
    pub capabilities: Capabilities,
}

enum WhitespaceFormat {
    SingleSpace,
    SingleTab,
    SingleTabOrSpace,
    Any,
}

impl Default for WhitespaceFormat {
    fn default() -> Self {
        Self::Any
    }
}

enum GameStateFormat {
    MovesSoFar,
    LastTurn,
    Nsn,
}

impl Default for GameStateFormat {
    fn default() -> Self {
        Self::Nsn
    }
}

fn parse_whitespace_format<'a>(it: &mut impl Iterator<Item = &'a str>) -> Result<WhitespaceFormat> {
    match it.next() {
        Some("single_space") => Ok(WhitespaceFormat::SingleSpace),
        Some("single_tab") => Ok(WhitespaceFormat::SingleTab),
        Some("single_tab_or_space") => Ok(WhitespaceFormat::SingleTabOrSpace),
        Some("any") => Ok(WhitespaceFormat::Any),
        Some(other) => Err(anyhow!("Unknown whitespace-identifier: {other}")),
        None => Err(anyhow!("Missing whitespace-identifier at end of line")),
    }
}

fn parse_game_state_format<'a>(it: &mut impl Iterator<Item = &'a str>) -> Result<GameStateFormat> {
    match it.next() {
        Some("moves_so_far") => Ok(GameStateFormat::MovesSoFar),
        Some("last_turn") => Ok(GameStateFormat::LastTurn),
        Some("nsn") => Ok(GameStateFormat::Nsn),
        Some(other) => Err(anyhow!("Unknown whitespace-identifier: {other}")),
        None => Err(anyhow!("Missing whitespace-identifier at end of line")),
    }
}

fn parse_bool<'a>(it: &mut impl Iterator<Item = &'a str>) -> Result<bool> {
    match it.next() {
        Some("true") => Ok(true),
        Some("false") => Ok(false),
        Some(other) => Err(anyhow!("Unknown boolean: {other}")),
        None => Err(anyhow!("Missing boolean at end of line")),
    }
}

fn parse_version<'a>(it: &mut impl Iterator<Item = &'a str>) -> Result<Version> {
    match it.next() {
        Some(v) => match v.split('.').collect::<Vec<_>>()[..] {
            [maj, min, pat] => Ok((maj.parse()?, min.parse()?, pat.parse()?)),
            _ => Err(anyhow!("version must be <major>.<minor>.<patch>, not {v}")),
        },
        None => Err(anyhow!("Missing version at end of line")),
    }
}

pub struct TimeControl {
    pub initial_time_w: Duration,
    pub initial_time_b: Duration,
    pub increment: Option<Duration>,
    pub delay: Option<Duration>,
    pub overtime: Vec<(i32, Duration)>,
    pub hourglass: bool,
    pub max_time_per_turn: Option<Duration>,
}

pub struct NeutrinoStandard {
    pub time_control: Option<TimeControl>,
    pub max_depth: Option<i32>,
    pub max_nodes: Option<i32>,
    pub move_limit: Option<i32>,
    pub custom_parameters: Vec<(String, Vec<String>)>,
}

pub enum GameParameters {
    NeutrinoStandard(NeutrinoStandard),
}

impl GameParameters {
    fn game_mode(&self) -> &str {
        match self {
            GameParameters::NeutrinoStandard(_) => "neutrino_standard",
        }
    }
}

pub struct Engine {
    pub info: EngineInfo,
    pub handle: EngineHandle,
}

pub struct Idling(Engine);

impl Idling {
    pub async fn quit(mut self) -> Result<ExitStatus> {
        self.0.handle.writeln("quit").await?;
        let status = self.0.handle.quit().await?;
        Ok(status)
    }

    pub async fn new_game(
        self,
        game_params: GameParameters,
        game_state: GameState,
        player: Player,
    ) -> Result<Playing> {
        let id = self.0.handle.process.id();
        match timeout(
            SETUP_TIMEOUT,
            self.new_game_inner(game_params, game_state, player),
        )
        .await
        {
            Ok(v) => v,
            Err(_) => Err(anyhow!(
                "Starting new game with engine (id = {id:?}) timed out"
            )),
        }
    }

    async fn new_game_inner(
        mut self,
        game_params: GameParameters,
        game_state: GameState,
        player: Player,
    ) -> Result<Playing> {
        self.0.info.capabilities.assert_support(&game_params)?;
        let handle = &mut self.0.handle;
        handle
            .writeln_noflush(format!("new_game {}", game_params.game_mode()).as_str())
            .await?;
        handle
            .writeln_noflush(format!("player {}", player).as_str())
            .await?;

        match &game_params {
            GameParameters::NeutrinoStandard(ns) => {
                if let Some(tc) = &ns.time_control {
                    let initial_time = match &player {
                        Player::W => tc.initial_time_w,
                        Player::B => tc.initial_time_b,
                    };
                    handle
                        .writeln_noflush(
                            format!("tc_initial_time {}", initial_time.as_millis()).as_str(),
                        )
                        .await?;
                    if let Some(increment) = tc.increment {
                        handle
                            .writeln_noflush(
                                format!("tc_increment {}", increment.as_millis()).as_str(),
                            )
                            .await?;
                    }
                    if let Some(delay) = tc.delay {
                        handle
                            .writeln_noflush(format!("tc_delay {}", delay.as_millis()).as_str())
                            .await?;
                    }
                    if tc.hourglass {
                        handle
                            .writeln_noflush(
                                format!(
                                    "tc_hourglass {} {}",
                                    tc.initial_time_w.as_millis(),
                                    tc.initial_time_b.as_millis()
                                )
                                .as_str(),
                            )
                            .await?;
                    }
                    if let Some(max) = tc.max_time_per_turn {
                        handle
                            .writeln_noflush(
                                format!("tc_max_per_turn {}", max.as_millis()).as_str(),
                            )
                            .await?;
                    }
                    for (ot_move, ot_increment) in &tc.overtime {
                        handle
                            .writeln_noflush(
                                format!("tc_overtime {ot_move} {} ", ot_increment.as_millis())
                                    .as_str(),
                            )
                            .await?;
                    }
                }
                if let Some(depth) = ns.max_depth {
                    handle
                        .writeln_noflush(format!("max_depth {depth}").as_str())
                        .await?;
                }
                if let Some(nodes) = ns.max_nodes {
                    handle
                        .writeln_noflush(format!("max_nodes {nodes}").as_str())
                        .await?;
                }
                for (cust_name, cust_pars) in &ns.custom_parameters {
                    let mut line = format!("custom_parameter {cust_name}");
                    for par in cust_pars {
                        write!(line, " {par}")?;
                    }
                    handle.writeln_noflush(&line).await?;
                }
            }
        }
        handle.writeln_noflush("new_game_parameters_end").await?;
        handle.flush().await?;

        let response = handle.readln().await?;
        if response != "ready_for_new_game" {
            return Err(anyhow!(
                "Engine did not respond with `ready_for_new_game`, but: {response}"
            ));
        }

        Ok(Playing {
            engine: self.0,
            game_params,
            game_state,
        })
    }
}

pub struct Playing {
    engine: Engine,
    game_params: GameParameters,
    // TODO(casper): Perhaps this should be behind Arc<Mutex<_>>
    game_state: GameState,
}

// TODO(casper): Consider adding state-machine-state Go'ing/Calculating, to
// indicate the engine is currently calculating its turn

impl Playing {
    pub async fn stop_game(self) -> Result<Idling> {
        let id = self.engine.handle.process.id();
        match timeout(STOP_GAME_TIMEOUT, self.stop_game_inner()).await {
            Ok(v) => v,
            Err(_) => Err(anyhow!("Stopping game with engine (id = {id:?}) timed out")),
        }
    }

    async fn stop_game_inner(mut self) -> Result<Idling> {
        self.engine.handle.writeln("stop_game").await?;

        let response = self.engine.handle.readln().await?;
        if response != "game_stopped" {
            return Err(anyhow!(
                "Engine did not respond with `game_stopped`, but: {response}"
            ));
        }

        Ok(Idling(self.engine))
    }
}
