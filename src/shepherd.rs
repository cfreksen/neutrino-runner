use std::process::{ExitStatus, Stdio};
use std::time::Duration;

#[cfg(any(target_os = "unix", target_os = "linux"))]
use std::os::unix::process::ExitStatusExt;
#[cfg(target_os = "windows")]
use std::os::windows::process::ExitStatusExt;

use anyhow::{anyhow, Result};
use log::{info, warn};
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader, BufWriter};
use tokio::process::{Child, ChildStderr, ChildStdin, ChildStdout, Command};
use tokio::time::timeout;

const QUIT_TIMEOUT: Duration = Duration::from_millis(2000);
const KILL_TIMEOUT: Duration = Duration::from_millis(4000);

#[derive(Debug)]
pub struct EngineFactory {
    pub expected_name: Option<String>,
    pub command: String,
    pub arg: String,
    pub engine_name: String,
}

impl EngineFactory {
    pub async fn start(&self, max_mem: usize, max_cores: u32) -> Result<EngineHandle> {
        dbg!(&self.expected_name);
        dbg!(&self.command);
        dbg!(&self.arg);

        // TODO(casper): Run this in some sort of sandbox. kata-container?
        // TODO(casper): Limit resource usage, possibly using cgroup

        let mut process = Command::new(&self.command)
            .arg(&self.arg)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;

        let engine_stdin = BufWriter::with_capacity(256, process.stdin.take().unwrap());
        let engine_stdout = BufReader::new(process.stdout.take().unwrap());
        let engine_stderr = process.stderr.take().unwrap();

        // TODO(casper): Spawn logger that listens to `engine_stderr`

        // TODO(casper): Store `engine_stdout` as list/queue of `(timestamp, line)`

        Ok(EngineHandle {
            process,
            engine_stdin,
            engine_stdout,
            engine_stderr,
            max_mem,
            max_cores,
            name: self.engine_name.to_owned(),
        })
    }
}

pub struct EngineHandle {
    pub process: Child,
    engine_stdin: BufWriter<ChildStdin>,
    engine_stdout: BufReader<ChildStdout>,
    engine_stderr: ChildStderr,
    pub max_mem: usize,
    pub max_cores: u32,
    pub name: String,
}

impl EngineHandle {
    // TODO(casper): Support `writeln!` macro instead. Consider how automatic flushing works then.
    pub async fn writeln(&mut self, msg: &str) -> Result<()> {
        self.engine_stdin.write_all(msg.as_bytes()).await?;
        self.engine_stdin.write_all(b"\n").await?;
        self.engine_stdin.flush().await?;
        Ok(())
    }

    pub async fn writeln_noflush(&mut self, msg: &str) -> Result<()> {
        self.engine_stdin.write_all(msg.as_bytes()).await?;
        self.engine_stdin.write_all(b"\n").await?;
        Ok(())
    }

    pub async fn flush(&mut self) -> Result<()> {
        Ok(self.engine_stdin.flush().await?)
    }

    pub async fn readln(&mut self) -> Result<String> {
        let mut buf = String::new();
        self.engine_stdout.read_line(&mut buf).await?;
        Ok(buf)
    }

    pub async fn quit(mut self) -> Result<ExitStatus> {
        let id = self.process.id();
        self.engine_stdin.shutdown().await?;

        match timeout(QUIT_TIMEOUT, self.process.wait()).await {
            Ok(Ok(v)) => return Ok(v),
            Ok(Err(e)) => warn!("Process (id = {id:?}) responded with error on quit: {e}"),
            Err(_) => info!("Quitting engine (id = {id:?}) timed out, killing it instead"),
        };

        match timeout(KILL_TIMEOUT, self.process.kill()).await {
            Ok(Ok(_)) => Ok(ExitStatus::from_raw(1)),
            Ok(Err(e)) => Err(anyhow!(e).context(format!(
                "Process (id = {id:?}) responded with error on kill"
            ))),
            Err(_) => Err(anyhow!("Killing engine (id = {id:?}) timed out")),
        }
    }
}
