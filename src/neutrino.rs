use std::{
    cmp::Ordering,
    fmt::{Display, Write},
};

use anyhow::{anyhow, Result};

pub enum Player {
    W,
    B,
}

impl Display for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            Player::W => f.write_char('W'),
            Player::B => f.write_char('B'),
        }
    }
}

pub enum Direction {
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW,
}

impl Direction {
    fn to_nan(&self) -> &'static str {
        match &self {
            Direction::N => "↑",
            Direction::NE => "↗",
            Direction::E => "→",
            Direction::SE => "↘",
            Direction::S => "↓",
            Direction::SW => "↙",
            Direction::W => "←",
            Direction::NW => "↖",
        }
    }
}

pub struct Pos {
    /// 0 => BBBBBC row, 4 => WWWWWC row
    row: i8,
    col: i8,
}

const COL_NAN: [char; 6] = ['a', 'b', 'c', 'd', 'e', 'f'];

impl Pos {
    fn to_nan(&self) -> Result<String> {
        Ok(format!(
            "{}{}",
            COL_NAN
                .get(self.col as usize)
                .ok_or_else(|| anyhow!("invalid col in position: {}", self.col))?,
            5 - self.row
        ))
    }

    fn from_nan(nan: &str) -> Result<Self> {
        let mut chars = nan.chars();
        let col_c = chars.next().ok_or_else(|| anyhow!("missing col char"))?;
        let row_c = chars.next().ok_or_else(|| anyhow!("missing row char"))?;
        if chars.next().is_some() {
            return Err(anyhow!("too many chars in NAN position"));
        }
        let col = COL_NAN
            .iter()
            .position(|c| c == &col_c)
            .ok_or_else(|| anyhow!("bad col char: {col_c}"))? as i8;
        let row = 5 - row_c
            .to_digit(10)
            .ok_or_else(|| anyhow!("bad row char: {row_c}"))? as i8;
        Ok(Self { row, col })
    }
}

pub struct PieceMove {
    from: Pos,
    to: Pos,
}

impl PieceMove {
    fn dir(&self) -> Result<Direction> {
        let d_col = self.from.col.cmp(&self.to.col);
        let d_row = self.from.row.cmp(&self.to.row);

        match (d_col, d_row) {
            (Ordering::Less, Ordering::Less) => Ok(Direction::NW),
            (Ordering::Less, Ordering::Equal) => Ok(Direction::N),
            (Ordering::Less, Ordering::Greater) => Ok(Direction::NE),
            (Ordering::Equal, Ordering::Less) => Ok(Direction::W),
            (Ordering::Equal, Ordering::Equal) => {
                Err(anyhow!("`from` and `to` are equal for piece move"))
            }
            (Ordering::Equal, Ordering::Greater) => Ok(Direction::E),
            (Ordering::Greater, Ordering::Less) => Ok(Direction::SW),
            (Ordering::Greater, Ordering::Equal) => Ok(Direction::S),
            (Ordering::Greater, Ordering::Greater) => Ok(Direction::SE),
        }
    }
}

pub enum Trilean<T> {
    Some(T),
    Unknown,
    None,
}

pub struct Turn {
    neutrino: Trilean<PieceMove>,
    player_piece: Trilean<PieceMove>,
}

impl Turn {
    fn to_nan(&self) -> Result<String> {
        let mut buf = String::new();
        match &self.neutrino {
            Trilean::Some(pm) => write!(buf, "{}", pm.to.to_nan()?)?,
            Trilean::Unknown => write!(buf, "_").unwrap(),
            Trilean::None => return Ok(String::new()),
        };
        match &self.player_piece {
            Trilean::Some(pm) => write!(buf, " {}{}", pm.dir()?.to_nan(), pm.to.to_nan()?)?,
            Trilean::Unknown => write!(buf, " _").unwrap(),
            Trilean::None => (),
        };
        Ok(buf)
    }
}

pub struct Move {
    turn_w: Turn,
    turn_b: Option<Turn>,
}

impl Move {
    fn to_nan(&self) -> Result<String> {
        match &self.turn_b {
            Some(b) => Ok(format!("{} {}", self.turn_w.to_nan()?, b.to_nan()?)),
            None => self.turn_w.to_nan(),
        }
    }
}

pub enum Piece {
    W,
    B,
    E,
    N,
    C,
}

impl Piece {
    fn as_char(&self) -> char {
        match &self {
            Piece::W => 'W',
            Piece::B => 'B',
            Piece::E => 'E',
            Piece::N => 'N',
            Piece::C => 'C',
        }
    }
}

pub struct Board {
    rows: [[Piece; 6]; 5],
}

pub enum Conclusion {
    Winner(Player),
    Draw,
}

pub struct GameState {
    moves: Vec<Move>,
    board: Board,
    current_player: Player,
    conclusion: Option<Conclusion>,
    unseen_moves: u64,
}

impl GameState {
    fn new() -> Self {
        type P = Piece;
        Self {
            moves: Vec::new(),
            board: Board {
                rows: [
                    [P::B, P::B, P::B, P::B, P::B, P::C],
                    [P::E, P::E, P::E, P::E, P::E, P::C],
                    [P::E, P::E, P::N, P::E, P::E, P::C],
                    [P::E, P::E, P::E, P::E, P::E, P::C],
                    [P::W, P::W, P::W, P::W, P::W, P::C],
                ],
            },
            current_player: Player::W,
            conclusion: None,
            unseen_moves: 0,
        }
    }

    fn nsn(&self) -> String {
        let board = self
            .board
            .rows
            .iter()
            .map(|row| row.iter().map(|s| s.as_char()).collect::<String>())
            .collect::<Vec<_>>()
            .join("/");

        let piece_to_move = match self.moves.last() {
            Some(mov) => {
                let turn = mov.turn_b.as_ref().unwrap_or(&mov.turn_w);
                match (&turn.neutrino, &turn.player_piece) {
                    (Trilean::None, _) => "N",
                    (_, Trilean::None) => "O",
                    _ => "N",
                }
            }
            None => "N",
        };
        let is_inside_move = if let Some(mov) = self.moves.last() {
            match &mov.turn_b {
                Some(tb) => match tb.player_piece {
                    Trilean::Some(_) => false,
                    Trilean::Unknown => false,
                    Trilean::None => true,
                },
                None => true,
            }
        } else {
            false
        };

        let seen_moves = self.moves.len() as u64 - if is_inside_move { 1 } else { 0 };
        format!(
            "{board} {}{piece_to_move} {}",
            self.current_player,
            self.unseen_moves + seen_moves
        )
    }

    fn moves_so_far(&self) -> Result<String> {
        if self.moves.is_empty() {
            return Ok("none".to_owned());
        }
        let mut buffer = String::new();
        for (idx, mov) in self.moves.iter().enumerate() {
            if idx > 0 {
                write!(buffer, " ").unwrap();
            }
            write!(
                buffer,
                "{}. {}",
                idx + 1 + self.unseen_moves as usize,
                mov.to_nan()?
            )
            .unwrap();
        }
        Ok(buffer)
    }

    fn last_turn(&self) -> Result<String> {
        match self.moves.last() {
            Some(mov) => match &mov.turn_b {
                Some(t) => t.to_nan(),
                None => mov.turn_w.to_nan(),
            },
            None => Ok("none".to_owned()),
        }
    }
}

impl Default for GameState {
    fn default() -> Self {
        Self::new()
    }
}
