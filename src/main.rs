use std::time::Duration;

use anyhow::Result;
use clap::Parser;

use neutrino::{GameState, Player};
use uni::{setup_engine, GameParameters, NeutrinoStandard, TimeControl};

mod neutrino;
mod shepherd;
mod uni;

#[derive(Parser, Debug)]
#[clap(author, version)]
struct Args {}

async fn gogo() -> Result<()> {
    let ef = shepherd::EngineFactory {
        expected_name: Some("dummy".to_owned()),
        command: "echo".to_owned(),
        arg: "qwe".to_owned(),
        engine_name: "Some name".to_owned(),
    };

    let handle = ef.start(1073741824, 1).await?;
    let idle = setup_engine(handle).await?;

    let gp = GameParameters::NeutrinoStandard(NeutrinoStandard {
        time_control: Some(TimeControl {
            initial_time_w: Duration::from_secs(3600),
            initial_time_b: Duration::from_secs(3600),
            increment: None,
            delay: None,
            overtime: Vec::new(),
            hourglass: false,
            max_time_per_turn: None,
        }),
        max_depth: None,
        max_nodes: None,
        move_limit: None,
        custom_parameters: Vec::new(),
    });
    let gs = GameState::default();
    let play = idle.new_game(gp, gs, Player::W).await?;

    // TODO(casper): Play some turns

    let idle = play.stop_game().await?;
    let status = idle.quit().await?;

    println!("Engine quit with status: {status}");

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    simple_logger::SimpleLogger::new().init().unwrap();
    let args = Args::parse();
    println!("Hello, world!");
    dbg!(&args);
    gogo().await?;

    Ok(())
}
